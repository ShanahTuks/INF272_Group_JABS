﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _272JABSProject.Models;

namespace _272JABSProject.ViewModels
{
    public class SignUpViewModel
    {


        //Data Members and Properties
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public System.DateTime DateOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int UserTypeID { get; set; }
        public string XCoordinate { get; set; }
        public string YCoordinate { get; set; }
        public int CurrentWeight { get; set; }
        public int GoalWeight { get; set; }
        public double Height { get; set; }
        public int GenderID { get; set; }

        [NotMapped]
        public List<Gender> GenderCollection { get; set; }

        public virtual UserType UserType { get; set; }
        public virtual Gender Gender{ get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    }
}