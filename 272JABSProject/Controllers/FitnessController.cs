﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _272JABSProject.Models;
using _272JABSProject.ViewModels;

namespace _272JABSProject.Controllers
{
    public class FitnessController : Controller
    {

        private FitnessEntities db = new FitnessEntities();

        // GET: Fitness
        public ActionResult Index()
        {
            return View();
        }

        //<<<<<<< HEAD
        //<<<<<<< HEAD
        //<<<<<<< HEAD
        public ActionResult UserProfile()
        {
            return View();
        }
        //<<<<<<< HEAD
        //=======

        public ActionResult FoodAnalysis()
        {
            return View();
        }
        //>>>>>>> Jackie'sBranch
        //=======
        public ActionResult ActivityInput()
        {
            return View();
        }

        public ActionResult FoodInput()
        {
            return View();
        }
        //>>>>>>> brendensbranch
        //=======

        public ActionResult ActivityAnalysis()
        {
            return View();
        }
        //>>>>>>> ShanahsBranch
        //=======
        [HttpGet]
        public ActionResult LogInPage()
        {

            return View();
        }

        [HttpPost]
        public ActionResult LogInPage(string Password , string Email)
        {



            return View();
        }

        [HttpGet]
        public ActionResult SignUpPage()
        {

            ViewBag.UserTypeID = new SelectList(db.UserTypes, "UserTypeID", "UserTypeName");
            ViewBag.GenderID = new SelectList(db.Genders, "GenderID", "GenderName");

            SignUpViewModel SignUp = new SignUpViewModel();
            //SignUp.GenderCollection = db.Genders.ToList<Gender>();

            return View(SignUp);
        }
        //>>>>>>> Amo'sBranch
        [HttpPost]
        public ActionResult SignUpPage([Bind(Include = "UserID,UserName,UserSurname,GenderID,DateOfBirth,Nationality,Email,Password,UserTypeID,XCoordinate,YCoordinate,CurrentWeight,GoalWeight,Height")] User NewUser)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(NewUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            
            return View();
        }
    }
}