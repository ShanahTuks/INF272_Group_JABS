USE [master]
GO
/****** Object:  Database [MindfulFitness]    Script Date: 2019/09/17 14:45:09 ******/
CREATE DATABASE [MindfulFitness]
GO
ALTER DATABASE [MindfulFitness] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MindfulFitness].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MindfulFitness] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MindfulFitness] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MindfulFitness] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MindfulFitness] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MindfulFitness] SET ARITHABORT OFF 
GO
ALTER DATABASE [MindfulFitness] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MindfulFitness] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MindfulFitness] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MindfulFitness] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MindfulFitness] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MindfulFitness] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MindfulFitness] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MindfulFitness] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MindfulFitness] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MindfulFitness] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MindfulFitness] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MindfulFitness] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MindfulFitness] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MindfulFitness] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MindfulFitness] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MindfulFitness] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MindfulFitness] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MindfulFitness] SET RECOVERY FULL 
GO
ALTER DATABASE [MindfulFitness] SET  MULTI_USER 
GO
ALTER DATABASE [MindfulFitness] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MindfulFitness] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MindfulFitness] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MindfulFitness] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MindfulFitness] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'MindfulFitness', N'ON'
GO
USE [MindfulFitness]
GO
/****** Object:  Table [dbo].[AmountEaten]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AmountEaten](
	[AmountID] [int] IDENTITY(1,1) NOT NULL,
	[DietID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Amount] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AmountEaten] PRIMARY KEY CLUSTERED 
(
	[AmountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CondtionType]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CondtionType](
	[ConditionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CondtionName] [varchar](50) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [PK_CondtionType] PRIMARY KEY CLUSTERED 
(
	[ConditionTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Conversation]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Conversation](
	[ConversationID] [int] IDENTITY(1,1) NOT NULL,
	[User1ID] [int] NOT NULL,
	[User2ID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_Conversation] PRIMARY KEY CLUSTERED 
(
	[ConversationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConversationReply]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConversationReply](
	[CRID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [text] NOT NULL,
	[UserID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[ConversationID] [int] NOT NULL,
 CONSTRAINT [PK_ConversationReply] PRIMARY KEY CLUSTERED 
(
	[CRID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Diet]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Diet](
	[DietID] [int] IDENTITY(1,1) NOT NULL,
	[DietName] [varchar](50) NOT NULL,
	[Description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Diet] PRIMARY KEY CLUSTERED 
(
	[DietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Excercises]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Excercises](
	[ExcerciseID] [int] IDENTITY(1,1) NOT NULL,
	[ExcericiseName] [varchar](50) NOT NULL,
	[ExcerciseDescription] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Excercises] PRIMARY KEY CLUSTERED 
(
	[ExcerciseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExcercisesDone]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExcercisesDone](
	[ExcerciseDoneID] [int] IDENTITY(1,1) NOT NULL,
	[ExcerciseID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Measurement] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ExcercisesDone] PRIMARY KEY CLUSTERED 
(
	[ExcerciseDoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReccomendedDiet]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReccomendedDiet](
	[ReccDietID] [int] IDENTITY(1,1) NOT NULL,
	[DietID] [int] NOT NULL,
	[Amount] [varchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_ReccomendedDiet] PRIMARY KEY CLUSTERED 
(
	[ReccDietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReccomendedExcercise]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReccomendedExcercise](
	[ReccExcerciseID] [int] IDENTITY(1,1) NOT NULL,
	[ExcerciseID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_ReccomendedExcercise] PRIMARY KEY CLUSTERED 
(
	[ReccExcerciseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[UserSurname] [varchar](50) NOT NULL,
	[GenderID] [int] NOT NULL,
	[DateOfBirth] [datetime] NOT NULL,
	[Nationality] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[UserTypeID] [int] NOT NULL,
	[XCoordinate] [varchar](50) NOT NULL,
	[YCoordinate] [varchar](50) NOT NULL,
	[CurrentWeight] [int] NOT NULL,
	[GoalWeight] [int] NOT NULL,
	[Height] [float] not null,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserConditions]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserConditions](
	[UserConditionID] [int] IDENTITY(1,1) NOT NULL,
	[ConditionTypeID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_UserConditions] PRIMARY KEY CLUSTERED 
(
	[UserConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserType]    Script Date: 2019/09/17 14:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeName] [varchar](50) NOT NULL,
	[Description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AmountEaten]  WITH CHECK ADD  CONSTRAINT [FK_AmountEaten_Diet] FOREIGN KEY([DietID])
REFERENCES [dbo].[Diet] ([DietID])
GO
ALTER TABLE [dbo].[AmountEaten] CHECK CONSTRAINT [FK_AmountEaten_Diet]
GO
ALTER TABLE [dbo].[AmountEaten]  WITH CHECK ADD  CONSTRAINT [FK_AmountEaten_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[AmountEaten] CHECK CONSTRAINT [FK_AmountEaten_User]
GO
ALTER TABLE [dbo].[Conversation]  WITH CHECK ADD  CONSTRAINT [FK_Conversation_User] FOREIGN KEY([User1ID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Conversation] CHECK CONSTRAINT [FK_Conversation_User]
GO
ALTER TABLE [dbo].[Conversation]  WITH CHECK ADD  CONSTRAINT [FK_Conversation_User1] FOREIGN KEY([User2ID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Conversation] CHECK CONSTRAINT [FK_Conversation_User1]
GO
ALTER TABLE [dbo].[ConversationReply]  WITH CHECK ADD  CONSTRAINT [FK_ConversationReply_Conversation] FOREIGN KEY([ConversationID])
REFERENCES [dbo].[Conversation] ([ConversationID])
GO
ALTER TABLE [dbo].[ConversationReply] CHECK CONSTRAINT [FK_ConversationReply_Conversation]
GO
ALTER TABLE [dbo].[ConversationReply]  WITH CHECK ADD  CONSTRAINT [FK_ConversationReply_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ConversationReply] CHECK CONSTRAINT [FK_ConversationReply_User]
GO
ALTER TABLE [dbo].[ExcercisesDone]  WITH CHECK ADD  CONSTRAINT [FK_ExcercisesDone_Excercises] FOREIGN KEY([ExcerciseID])
REFERENCES [dbo].[Excercises] ([ExcerciseID])
GO
ALTER TABLE [dbo].[ExcercisesDone] CHECK CONSTRAINT [FK_ExcercisesDone_Excercises]
GO
ALTER TABLE [dbo].[ExcercisesDone]  WITH CHECK ADD  CONSTRAINT [FK_ExcercisesDone_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ExcercisesDone] CHECK CONSTRAINT [FK_ExcercisesDone_User]
GO
ALTER TABLE [dbo].[ReccomendedDiet]  WITH CHECK ADD  CONSTRAINT [FK_ReccomendedDiet_Diet] FOREIGN KEY([DietID])
REFERENCES [dbo].[Diet] ([DietID])
GO
ALTER TABLE [dbo].[ReccomendedDiet] CHECK CONSTRAINT [FK_ReccomendedDiet_Diet]
GO
ALTER TABLE [dbo].[ReccomendedDiet]  WITH CHECK ADD  CONSTRAINT [FK_ReccomendedDiet_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ReccomendedDiet] CHECK CONSTRAINT [FK_ReccomendedDiet_User]
GO
ALTER TABLE [dbo].[ReccomendedExcercise]  WITH CHECK ADD  CONSTRAINT [FK_ReccomendedExcercise_Excercises] FOREIGN KEY([ExcerciseID])
REFERENCES [dbo].[Excercises] ([ExcerciseID])
GO
ALTER TABLE [dbo].[ReccomendedExcercise] CHECK CONSTRAINT [FK_ReccomendedExcercise_Excercises]
GO
ALTER TABLE [dbo].[ReccomendedExcercise]  WITH CHECK ADD  CONSTRAINT [FK_ReccomendedExcercise_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ReccomendedExcercise] CHECK CONSTRAINT [FK_ReccomendedExcercise_User]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserType] FOREIGN KEY([UserTypeID])
REFERENCES [dbo].[UserType] ([UserTypeID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserType]
GO
ALTER TABLE [dbo].[UserConditions]  WITH CHECK ADD  CONSTRAINT [FK_UserConditions_CondtionType] FOREIGN KEY([ConditionTypeID])
REFERENCES [dbo].[CondtionType] ([ConditionTypeID])
GO
ALTER TABLE [dbo].[UserConditions] CHECK CONSTRAINT [FK_UserConditions_CondtionType]
GO
ALTER TABLE [dbo].[UserConditions]  WITH CHECK ADD  CONSTRAINT [FK_UserConditions_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[UserConditions] CHECK CONSTRAINT [FK_UserConditions_User]
GO
USE [master]
GO
ALTER DATABASE [MindfulFitness] SET  READ_WRITE 
GO
